/**
 * Master application
 */


 /**
  * TODO: Router on hashchange, bind/unbind events: login,register; **extra: models
  */
window.Env = window.Env || {};

(function(GLOBAL, DOC, LIST){

        //querySelectorAll

        // window.onhashchange = onHashChange();
        var location = window.location;
        var hash =  location.hash;
        var uri = location.href;
        var navLinksObj = document.querySelectorAll('.menu-links');
        var navActiveLink = document.querySelectorAll('.active');

        // console.log('/navLinksObj/-> /' + navLinksObj + '/');
        // console.log('/navActiveLink/-> /' + navActiveLink + '/');
        // console.log('/hash/-> /' + hash + '/');

        for (var i = 0; i <= 0; i++) {
            navLinksObj.length = i;
            // console.log('navLinksObj: ' + navLinksObj[i]);
            // console.log('navLinksObj: ' + navLinksObj[0]);

            document.addEventListener('click', function () {

                hash = window.location.hash;
                var hashClear = hash.replace("#", '');

                console.log(navLinksObj);

                navLinksObj = document.getElementsByTagName("a");

                console.log(navLinksObj[0] + ' /el 0');
                console.log(navLinksObj[1] + ' /el 1');
                console.log(navLinksObj[2] + ' /el 2');

                var navActiveLink  = navLinksObj[0].classList.remove('active');
                var currActiveLink = document.getElementsByTagName("a")[i];
                // var currActive     = navLinksObj[i].classList.add('active');

                var currActiveLink2 = document.getElementsByTagName("a").href = hash;
                var currActive     = navLinksObj[i].classList.add('active');
                console.log('/_________/'+ currActive);

                console.log('/--------- globals ----------------');
                console.log('>>>>>>>>>>>>>>>>> ' + i + ' <<<<<<<<<<<<<');
                console.log('navLinksObj');
                console.log(navLinksObj);
                console.log('navActiveLink');
                console.log(navActiveLink);
                console.log('/--------- loops for link ---------');
                console.log('=> navLinksObj[i]');
                console.log(navLinksObj[i]);
                console.log('/---');
                console.log('=> navActiveLink[i]');
                console.log(navActiveLink);
                console.log('/---');
                console.log('=> currActiveLink ------------//////');
                console.log(currActiveLink);
                console.log('=> currActiveLink2');
                console.log(currActiveLink2);
                console.log('=> currActive');
                console.log(currActive);
                console.log('/---');
                console.log('=> hashClear');
                console.log(hashClear);
                console.log('/----------------------------------');
            })
        };


        // function onHashChange () {

        //     var location = window.location;
        //     var hash =  location.hash;

        //     var hashClear = hash.replace("#"," ");

        //     var navLinksAll = document.querySelectorAll('.menu-links');
        //     var activeLink = document.getElementsByClassName('.active');

        //     // var newLink = document.getElementsByClassName('.menu-links');
        //     var elementByClassName = document.getElementByClassName;

        //     // !== operator will return true or false, depends on whats is current hash (string or number, from previous page).
        //     // we expect in our case to be a hashtag/page, so with #page its going to be fine as a string taken (hope so).
        //     if ( hashClear !== '' ) { //this will return false in empty hash(redirect).
        //         console.log(hashClear);
        //         // var curLink = activeLink[0].classList.remove('.active');
        //         // var newLink = curLink.addClass('.active');

        //         // var removeActive = activeLink.removeClass('active');
        //         alert('current hash://' + hashClear);

        //         var curLink = oldLink.addClass('.active');
        //     }

        //     else {
        //         console.log(hashClear);
        //         // var curLink = oldLink.addClass('.active');
        //         // var activeLink = curLink.removeClass('.active');

        //         alert('old hash://' + hashClear);
        //     }
        // }
        // onHashChange();

        function LIST() {
            document.addEventListener("hashchange", onHashChange());
        };

        // console.log(activeLink);


        GLOBAL.Router
        .config({
            root: '/login',
            has404: true
        })
        .add('/404', function() {
            var error404View = new BaseView();
            error404View.create({id: 'placeholder-404', template: '404'});
        })
        .add('/register', function() {
            var registerView = new BaseView();
            registerView
            .create({id: 'placeholder-register', template: 'register'})
            .unbindEvents({'#login submit': GLOBAL.loggedUser})
            .bindEvents({'#register submit': GLOBAL.registerUser});
        })
        .add('/login', function() {
            var loginView = new BaseView();
            loginView
            .create({id: 'placeholder-login', template: 'login'})
            .unbindEvents({'#register submit': GLOBAL.registerUser})
            .bindEvents({'#login submit': GLOBAL.loggedUser});
        })
        .add('/home', function() {
            var homeView = new BaseView();
            homeView.create({id: 'placeholder-home', template: 'home'});
        })
        .start();

})(window, document, function LIST(){});