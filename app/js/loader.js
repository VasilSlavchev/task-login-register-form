/***
* loadHTML - Fetches the needed template and renders it
* @param {Stirng} templateName - name of the template to be retrieved
* returns {String}
*/
function loadHTML(templateName, callback) {
    if (window.XMLHttpRequest) {
        ajaxRequest = new XMLHttpRequest();
    }

    ajaxRequest.onreadystatechange = function () {
        if (ajaxRequest.readyState === 4 ) {
            if (ajaxRequest.status === 200 && ajaxRequest.status < 300) {
                //return the html of the template as plain string
                callback && callback(ajaxRequest.responseText);
            }
        }
    };
    //console.log('LOADER: '+ typeof templateName);
    //get the needed template
    ajaxRequest.open("GET", "app/templates/" + templateName + ".html", true);
    ajaxRequest.send();

}


