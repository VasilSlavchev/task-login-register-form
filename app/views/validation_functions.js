/**
 * Checks element's length
 *
 * @param {String} elementName  element's name
 * @param {String} form         name of form which content element
 * @param {Object} errors       object which content errors messages
 * @param {Number} min          Minimum element's length
 * @param {String} lable        name of the error
 *
 * @return {Object} errors      object which content errors messages
 */
function checkLength (elementName, form, errors, min, lable) {

    min = min || 0;

    var element = document.forms[form][elementName].value.trim();

    if(element.length < min) {
        errors[elementName] = lable + " must be " + min + " long";
    }

    return errors;
}

/**
 * Checks if the element contains special characters
 *
 * @param {String} elementName  element's name
 * @param {String} form         name of form which content element
 * @param {Object} errors       object which content errors messages
 * @param {String} lable        name of the error
 *
 * @return {Object} errors      object which content errors messages
 */

function checkCharacters (elementName, form, errors, lable) {

    var element = document.forms[form][elementName].value.trim();

    if(/[^a-zA-Z0-9\-\/]/.test(element)) {
        errors[elementName] = lable + " must not contain special characters!";
    }

    return errors;
}


/**
 * Checks if the element1 and element2 are equal
 *
 * @param {String} elementName1  first element's name
 * @param {String} elementName2  second element's name
 * @param {String} form          name of form which content element
 * @param {Object} errors        object which content errors messages
 * @param {String} lable         name of the error
 *
 * @return {Object} errors       object which content errors messages
 */
function checkRepeat (elementName1, elementName2, form, errors, lable) {

    var element1 = document.forms[form][elementName1].value.trim(),
        element2 = document.forms[form][elementName2].value.trim();

    if(element1 !== element2) {
        errors["confirm_password"] = lable + " don't match!";
    }

    return errors;
}

